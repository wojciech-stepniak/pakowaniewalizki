package stepniak.wojtek.pakowaniewalizki;

import java.util.List;

import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.iface.IValidationCallback;

/**
 * Created by Wojtek on 2015-06-11.
 */
public class ValidationEmptyCallback implements IValidationCallback {

  @Override
  public void validationComplete(boolean result, List<FormValidator.ValidationFail> failedValidations) {

  }
}
