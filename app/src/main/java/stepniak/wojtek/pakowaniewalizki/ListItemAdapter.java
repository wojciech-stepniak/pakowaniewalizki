package stepniak.wojtek.pakowaniewalizki;

import stepniak.wojtek.pakowaniewalizki.model.Item;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Wojtek on 2015-05-28.
 */
public class ListItemAdapter extends ArrayAdapter<Item> {

  private final Context context;
  private final ArrayList<Item> items;
  private int resourceView;

  public ListItemAdapter(Context context, ArrayList<Item> items, int resourceView) {
    super(context, -1, items);
    this.context = context;
    this.items = items;
    this.resourceView = resourceView;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View rowView = inflater.inflate(resourceView, parent, false);
    TextView titleTextView = (TextView) rowView.findViewById(R.id.item_title);
    titleTextView.setText(items.get(position).getName());

    TextView weightTextView = (TextView) rowView.findViewById(R.id.item_weight);
    weightTextView.setText(items.get(position).getWeightToScreen());

    TextView valueTextView = (TextView) rowView.findViewById(R.id.item_value);
    valueTextView.setText(items.get(position).getPriceToScreen());

    // change the icon for Windows and iPhone
    //String s = values[position];
    return rowView;
  }


}
