package stepniak.wojtek.pakowaniewalizki;

import android.app.LauncherActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import stepniak.wojtek.pakowaniewalizki.model.Item;
import stepniak.wojtek.pakowaniewalizki.model.Suitcase;


public class ResultPackActivity extends ActionBarActivity {

  private Suitcase packedSuitcase = null;
  private final String TAG = "ResultPackActivity";
  @InjectView(R.id.result_list_view)
  ListView resultListView;
  @InjectView(R.id.activity_result_total_value)
  TextView totalValue;
  @InjectView(R.id.activity_result_total_weight)
  TextView totalWeight;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_result_pack);
    ButterKnife.inject(this);

    Bundle bundle = getIntent().getExtras();
    if (bundle != null && bundle.containsKey("packedSuitcase")) {
      packedSuitcase = (Suitcase) bundle.getSerializable("packedSuitcase");
      Log.i(TAG, packedSuitcase.toString());

      ListItemAdapter listAdapter = new ListItemAdapter(this, packedSuitcase.getContents(), R.layout.result_item_layout);
      resultListView.setChoiceMode(ListView.CHOICE_MODE_NONE);
      resultListView.setItemsCanFocus(false);
      resultListView.setAdapter(listAdapter);

      totalWeight.setText(packedSuitcase.getTotalWeight());
      totalValue.setText(packedSuitcase.getTotalValue());

    } else if (bundle != null) {
      Log.i(TAG, "I not receive bundle with result packing");
    }
  }


  @Override
  protected void onResume() {
    super.onResume();
    //Log.i(TAG, packedSuitcase.toString());
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_result_pack, menu);
    return false;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
