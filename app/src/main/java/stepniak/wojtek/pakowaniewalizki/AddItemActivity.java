package stepniak.wojtek.pakowaniewalizki;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.MaxLength;
import eu.inmite.android.lib.validations.form.annotations.MinLength;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.annotations.RegExp;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;
import stepniak.wojtek.pakowaniewalizki.model.Item;


public class AddItemActivity extends ActionBarActivity {

  @NotEmpty(messageId = R.string.validator_title_empty, order = 1)
  @MinLength(value = 3, messageId = R.string.validator_title_short, order = 2)
  @InjectView(R.id.newNameItem)
  EditText item_title;

  @NotEmpty(messageId = R.string.validator_weight_empty, order = 1)

  @RegExp(value = "^([0-9]*)?(\\.[0-9]?)?$", messageId = R.string.validator_weight_one_digit, order = 2)
  @MaxLength(value = 5, messageId = R.string.validator_weight_too_much, order = 3)
  @InjectView(R.id.newWeightItem)
  EditText item_weight;

  @NotEmpty(messageId = R.string.validator_value_empty, order = 1)
  @RegExp(value = "^([0-9]*)?(\\.[0-9]{0,2})?$", messageId = R.string.validator_value_format, order = 2)
  @InjectView(R.id.newPriceItem)
  EditText item_price;

  @InjectView(R.id.activity_add_item_layout)
  View activity_layout;

  final String TAG = "AddItemActivity";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_add_item);
    ButterKnife.inject(this);

    FormValidator.startLiveValidation(this, activity_layout, new SimpleErrorPopupCallback(this));
  }

  @OnClick(R.id.button_add_item)
  public void addItem(Button button) {
    // Walidacja formularza
    if (FormValidator.validate(this, new ValidationEmptyCallback())) {
      // Walidacja OK
      Log.i(TAG, "Walidacja OK");
      String title = item_title.getText().toString();
      int price = (int)(Double.parseDouble(item_price.getText().toString()) * 100);
      int weight = (int)(Double.parseDouble(item_weight.getText().toString()) * 10);
      Item newItem = new Item(item_title.getText().toString(), price, weight);

      Intent returnIntent = new Intent();
      returnIntent.putExtra("result", newItem);
      setResult(RESULT_OK, returnIntent);
      finish();
    } else {
      Log.i(TAG, "Walidacja Błąd");
    }

    // TODO Powrót do listy
  }




  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_add_item, menu);
    return false;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
