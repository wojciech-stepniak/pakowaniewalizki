package stepniak.wojtek.pakowaniewalizki;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.MaxLength;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.annotations.RegExp;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;
import stepniak.wojtek.pakowaniewalizki.model.Item;
import stepniak.wojtek.pakowaniewalizki.model.Suitcase;


public class ListItemActivity extends ActionBarActivity {

  static final int ContextMenu_Delete = 1;
  static final int ContextMenu_Edit = 2;

  static final int Activity_Add = 101;
  static final int Activity_Edit = 102;

  @InjectView(R.id.activity_list_layout)
  View activity_list_layout;

  private ListView list;
  private static final String TAG = "ListItemActivity";
  @NotEmpty(messageId = R.string.validator_capacity_empty, order = 1)
  @RegExp(value = "^([0-9]*)?(\\.[0-9]{1})?$", messageId = R.string.validator_capacity_one_digit, order = 2)
  @MaxLength(value = 5, messageId = R.string.validator_capacity_too_much, order = 3)
  @InjectView(R.id.capacitySuitcase)
  EditText capacitySuitcase;



  @InjectView(R.id.button_pack)
  Button packButton;

  private ArrayList<Item> listItems;
  private ListItemAdapter adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    ActiveAndroid.initialize(this);

    listItems = Item.getAll();

    setContentView(R.layout.activity_list_item);
    ButterKnife.inject(this);
    FormValidator.startLiveValidation(this, activity_list_layout, new SimpleErrorPopupCallback(this));
    list = (ListView) findViewById(R.id.listView);


    adapter = new ListItemAdapter(this, listItems, R.layout.select_item_layout);

    list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
    list.setItemsCanFocus(false);

    registerForContextMenu(list);
    list.setAdapter(adapter);

  }

  @OnClick(R.id.button_pack)
  public void pack(Button button) {
    Log.i(TAG, "Kliknieto");
    // Perform action on click
/*    StringBuilder sb = new StringBuilder();
    SparseBooleanArray ids = list.getCheckedItemPositions();
    for (int i = 0; i < list.getCount(); i++) {
      if (ids.get(i)) {
        sb.append(" " + i);
      }
    }
    Log.i(TAG, "Wybranych : " + list.getCheckedItemCount());
    Log.i(TAG, "Wybrane: " + sb.toString());*/
    // Walidacja formularza
    if (FormValidator.validate(ListItemActivity.this, new ValidationEmptyCallback())) {
      packItemsToSuitcase();
    } else {
      Log.i(TAG, "Walidacja nieudana");
      capacitySuitcase.setFocusableInTouchMode(false);
      capacitySuitcase.setFocusable(false);
      capacitySuitcase.setFocusableInTouchMode(true);
      capacitySuitcase.setFocusable(true);
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    // TODO Tutaj odswiez liste przedmiotow, ew. jeszcze pojemnosc walizki zaczytaj
    adapter.notifyDataSetChanged();
    Log.i(TAG, "onResume");
  }

  private void packItemsToSuitcase() {
    ArrayList<Item> selectedItems = new ArrayList<Item>();
    SparseBooleanArray ids = list.getCheckedItemPositions();
    int capacity;
    if (capacitySuitcase.getText().toString().contains(".")) {
      String[] betweenDot = capacitySuitcase.getText().toString().split("\\.");
      int before = 0;
      int after = 0;
      if (betweenDot[0] != null && betweenDot[0].length() > 0) {
        before = Integer.parseInt(betweenDot[0]);
      }
      if (betweenDot[1] != null && betweenDot[1].length() > 0) {
        after = Integer.parseInt(betweenDot[1]);
      }
      capacity = before * 10 + after;
    } else {
      capacity = Integer.parseInt(capacitySuitcase.getText().toString()) * 10;
    }

    for (int i = 0; i < list.getCount(); i++) {
      if (ids.get(i)) {
        selectedItems.add((Item)list.getItemAtPosition(i));
      }
    }
    Suitcase suitcase = new Suitcase("Walizka", capacity);
    suitcase.pack(selectedItems);
    Log.i(TAG, suitcase.toString());


    // Przekazmy kolekcje wynikow do nastepnej aktywnosci

    Intent resultActivity = new Intent(getApplicationContext(), ResultPackActivity.class);
    resultActivity.putExtra("packedSuitcase", suitcase);
    startActivity(resultActivity);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_list_item, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      Log.i(TAG, "Wybrano opcje z MENU");
      Intent intent = new Intent(this, AddItemActivity.class);
      /*EditText editText = (EditText) findViewById(R.id.edit_message);
      String message = editText.getText().toString();
      intent.putExtra(EXTRA_MESSAGE, message);*/
      startActivityForResult(intent, Activity_Add);

      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
    super.onCreateContextMenu(menu, v, menuInfo);

    if (v.getId() == R.id.listView) {
      ListView listView = (ListView) v;
      AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
      Item selectedItem = (Item) listView.getItemAtPosition(acmi.position);

      menu.add(Menu.NONE, ContextMenu_Delete, Menu.NONE, "Usuń");
      menu.add(Menu.NONE, ContextMenu_Edit, Menu.NONE, "Edytuj");
    }
  }

  @Override
  public boolean onContextItemSelected(MenuItem item) {
    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
    int index = info.position;
    switch(item.getItemId()) {
      case ContextMenu_Delete:
        Item toRemoveItem = listItems.get(index);
        listItems.remove(index);
        toRemoveItem.delete();
        adapter.notifyDataSetChanged();
        Toast.makeText(this, "Usunięto " + index, Toast.LENGTH_SHORT).show();
        break;
      case ContextMenu_Edit:
        Toast.makeText(this, "Edytuje " + index, Toast.LENGTH_SHORT).show();
        Item toEditItem = listItems.get(index);
        // uruchom aktywnosc z edycja
        Intent resultActivity = new Intent(this, EditItemActivity.class);
        resultActivity.putExtra("toEditItem", toEditItem);
        startActivityForResult(resultActivity, Activity_Edit);
        break;
    }
    return true;
  }

  // Zwraca nowy Item z ekranu dodania
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    if (requestCode == Activity_Add) {
      if(resultCode == RESULT_OK){
        Item result = (Item) data.getSerializableExtra("result");
        result.save();
        listItems.add(result);
        adapter.notifyDataSetChanged();
      }
      if (resultCode == RESULT_CANCELED) {
        //Write your code if there's no result
      }
    } else if (requestCode == Activity_Edit) {
      if(resultCode == RESULT_OK){
        Item oldItem = (Item) data.getSerializableExtra("oldItem");
        Item newItem = (Item) data.getSerializableExtra("newItem");
        Log.i(TAG, listItems.indexOf(oldItem) + "");

        Item toChangeItem = new Select().from(Item.class).where("name = ?", oldItem.getName())
                .where("value = ?", oldItem.getValue())
                .where("cost = ?", oldItem.getCost()).executeSingle();

        toChangeItem.setName(newItem.getName());
        toChangeItem.setValue(newItem.getValue());
        toChangeItem.setCost(newItem.getCost());
        toChangeItem.save();

        listItems.set(listItems.indexOf(oldItem), toChangeItem);
        /*
        oldItem.delete();
        newItem.save();
        adapter.notifyDataSetChanged();*/
      }
      if (resultCode == RESULT_CANCELED) {
        //Write your code if there's no result
      }
    }
  }//onActivityResult
}
