package stepniak.wojtek.pakowaniewalizki.controller;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import stepniak.wojtek.pakowaniewalizki.model.Item;
import stepniak.wojtek.pakowaniewalizki.model.Suitcase;

public class DynamicKnapsack {

  /**
   * Zwraca list� rzeczy, kt�re najbardziej op�aca si� zapakowa� do walizki
   *
   * @param suitcase walizka do spakowania
   * @param items    rzeczy dostepne do spakowania
   * @return List<Item>
   */
  public static ArrayList<Item> tryPack(Suitcase suitcase, List<Item> items) {
    int capacity = suitcase.getCapacity();
    int numberOfItems = items.size();
    // nalezy pamietac ze tablice domyslnie inicjowane zerami
    int valuableMatrix[][] = new int[numberOfItems + 1][capacity + 1];
    int keepMatrix[][] = new int[numberOfItems + 1][capacity + 1]; // przechowuje id przedmiotow pakowanych jako ostatnie

    for (int n = 1; n <= numberOfItems; n++) { // dla kazdego przedmiotu
      for (int c = 1; c <= capacity; c++) { // dla kazdej mozliwej pojemnosci plecaka
        int option1 = valuableMatrix[n - 1][c];

        int option2 = Integer.MIN_VALUE;
        Item item = items.get(n - 1); // i-ty element
        if (item.getCost() <= c)
          option2 = item.getValue() + valuableMatrix[n - 1][c - item.getCost()];

        // wybor lepszej opcji
        if (option2 > option1) {
          valuableMatrix[n][c] = option2;
          keepMatrix[n][c] = 1;
        } else {
          valuableMatrix[n][c] = option1;
          keepMatrix[n][c] = 0;
        }
      }
    }

    //  DLA CELOW DEBUGOWANIA Wyswietl macierze
    System.out.println("Macierz zysku:");
    for (int n = 1; n <= numberOfItems; n++) { // dla kazdego przedmiotu
      for (int c = 1; c <= capacity; c++) {
        System.out.printf("%5s ", valuableMatrix[n][c]);
      }
      System.out.println();
    }

    System.out.println("Macierz elementow:");
    for (int n = 1; n <= numberOfItems; n++) { // dla kazdego przedmiotu
      for (int c = 1; c <= capacity; c++) {
        System.out.printf("%5s ", keepMatrix[n][c]);
      }
      System.out.println();
    }
    //  Zwroc nowa kolekcje
    ArrayList<Item> optimalItems = new ArrayList<Item>();
    int indexItemToPack = numberOfItems;
    int actualProbingCapacity = capacity;
    while(indexItemToPack > 0) {
      if (keepMatrix[indexItemToPack][actualProbingCapacity] == 1) {
        optimalItems.add(items.get(indexItemToPack-1));
        actualProbingCapacity -= items.get(indexItemToPack-1).getCost();
      }
      indexItemToPack--;
    }

    return optimalItems;
  }

}
