package stepniak.wojtek.pakowaniewalizki.model;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import stepniak.wojtek.pakowaniewalizki.controller.DynamicKnapsack;

public class Suitcase implements Serializable {

  private String name;
  private int capacity;
  private ArrayList<Item> contents;

  NumberFormat priceFormat = new DecimalFormat("#0.00");
  NumberFormat weightFormat = new DecimalFormat("#0.0");

  public Suitcase(String name, int capacity) {
    this.name = name;
    this.capacity = capacity;
    this.contents = new ArrayList<Item>();
  }

  public String getName() {
    return name;
  }

  public int getCapacity() {
    return capacity;
  }

  public ArrayList<Item> getContents() {
    return contents;
  }

  /**
   * Metoda probuje zapakowac items do biezacej walizki
   *
   * @param items
   */
  public void pack(List<Item> items) {
    // TODO Wywolanie algorytmu pakowania plecaka
    this.contents = DynamicKnapsack.tryPack(this, items);
  }

/*  *//**
   * Metoda ma splaszczyc liste obiektow
   *//*
  public Suitcase reduce() {
    Suitcase s = new Suitcase(this.getName(), this.getCapacity());
    ArrayList<Item> reduceItems = new ArrayList<Item>();
    // jesli pare razy ten sam przedmiot sie pojawia to go wrzuc raz i zwieksz counta
    for (Item item: this.contents) {
      int index = reduceItems.indexOf(item);
      if (index >= 0) {
        Item existsItem = reduceItems.get(index);
        existsItem.increaseCount();
      } else {
        Item copyItem = new Item(item.getName(), item.getValue(), item.getCost(), 1);
        reduceItems.add(copyItem);
      }
    }
    s.contents = reduceItems;
    return s;
  }*/

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("Do walizki: " + this.name + " o pojemnosci: " + this.capacity
            + " spakowano: \n");
    double totalCost = 0;
    double totalValue = 0;
    for (Item item : this.contents) {
      totalCost += item.getCost();
      totalValue += item.getValue();
      sb.append(item.getName() + "(waga, wartosc) = (" + item.getCost() + ", " + item.getValue() + ") \n");
    }
    sb.append("---\n");
    sb.append("Razem(waga, wartosc) = (" + totalCost + ", " + totalValue + ")\n");
    return sb.toString();
  }

  public String getTotalWeight() {
    int sum = 0;
    for (Item i: getContents()) {
      sum += i.getCost();
    }
    return weightFormat.format((double)sum / 10) + " kg / " +
            weightFormat.format((double)getCapacity() / 10) + " kg";
  }

  public String getTotalValue() {
    int sum = 0;
    for (Item i: getContents()) {
      sum += i.getValue();
    }
    return priceFormat.format((double) sum / 100) + " PLN";
  }


}
