package stepniak.wojtek.pakowaniewalizki.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Table(name = "Items")
public class Item extends Model implements Serializable {

  @Column(name = "Name")
  private String name;
  @Column(name = "Value")
  private int value; // zysk
  @Column(name = "Cost")
  private int cost; // waga

  NumberFormat priceFormat = new DecimalFormat("#0.00");
  NumberFormat weightFormat = new DecimalFormat("#0.0");

  // for ActiveAndroid
  public Item() {
    super();
  }

  public Item(String name, int value, int cost) {
    this.name = name;
    this.value = value;
    this.cost = cost;
  }

  public String getName() {
    return name;
  }

  public int getValue() {
    return value;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setValue(int value) {
    this.value = value;
  }

  public void setCost(int cost) {
    this.cost = cost;
  }

  public int getCost() {
    return cost;
  }

  public String getWeightToScreen() {
    return weightFormat.format((double)getCost() / 10) + " kg";
  }

  public String getWeightToEditForm() {
    return weightFormat.format((double)getCost() / 10).replace(',', '.');
  }

  public String getPriceToScreen() {
    return priceFormat.format((double) getValue() / 100) + " PLN";
  }

  public String getPriceToEditForm() {
    return priceFormat.format((double) getValue() / 100).replace(',', '.');
  }

  public static ArrayList<Item> exampleSet() {
    Item koszula = new Item("Koszula", 75, 7);
    Item spodnie = new Item("Spodnie", 150, 8);
    Item sweter = new Item("Sweter", 250, 6);
    Item czapka = new Item("Czapka", 35, 4);
    Item kapielowki = new Item("Kapielowki", 10, 3);
    Item buty = new Item("Buty", 100, 9);

    ArrayList<Item> mozliweDoSpakowania = new ArrayList<Item>();
    mozliweDoSpakowania.add(koszula);
    mozliweDoSpakowania.add(spodnie);
    mozliweDoSpakowania.add(sweter);
    mozliweDoSpakowania.add(czapka);
    mozliweDoSpakowania.add(kapielowki);
    mozliweDoSpakowania.add(buty);

    return mozliweDoSpakowania;
  }

  public static ArrayList<Item> getAll() {
    List<Item> items = new Select()
            .from(Item.class)
            .orderBy("Name ASC")
            .execute();
    return new ArrayList<Item>(items);
  }

  @Override
  public boolean equals(Object object)
  {
    boolean sameSame = false;
    if (object != null && object instanceof Item)
    {
      sameSame = this.value == ((Item) object).value;
    }
    return sameSame;
  }

}
